FROM richarvey/nginx-php-fpm:latest
RUN php -v
COPY ./service1/index.php /var/www/html/index.php
CMD ["/usr/sbin/httpd","-D","FOREGROUND"]
EXPOSE 80
